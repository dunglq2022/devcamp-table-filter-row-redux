
const initialState = {
    inputFilter: '', //Giá trị của ô input
    rows: [{stt: '1', content: 'Cà phê'}, {stt: '2', content: 'Trà tắc'}, {stt: '3', content: 'Pepsi'}, {stt: '4', content: 'Cocacola'}, {stt: '5', content: 'Trà sữa'}, {stt: '6', content: 'Matcha'}, {stt: '7', content: 'Hồng trà'}, {stt: '8', content: 'Trà xanh kem cheese'}, {stt: '9', content: 'Trà đá'}],
    filter: [] //Danh sách dữ liệu
};


const filterEvent = (state = initialState, action) => {
    switch (action.type) {
        case "VALUE_HANDLER": {
            return {
                ...state,
                inputFilter: action.payload.inputFilter
            };
        }
        
        case "FILTER_ROWS": {
            const inputFilter = state.inputFilter;
            let filter = [];
            if(inputFilter !== '') {
                //Kiểm tra input có chứa key content
                filter = state.rows.filter((key) => {
                    return key.content.toLowerCase().includes(inputFilter.toLowerCase())
                });
                console.log(filter)
            }
            return {
                ...state,
                filter: filter,
            }
        }
        default: {
            return state;
        }
    }
};

export default filterEvent;
