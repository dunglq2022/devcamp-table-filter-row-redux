import { combineReducers, createStore } from "redux";
import tableRowFilterEven from '../components/TableRowFilterEven'
const appReducer = combineReducers({
    filterReducer: tableRowFilterEven
})

const store = createStore(
    appReducer,
);

export default store;